import Profile from "../pages/Profile"
import Dashboard from "../pages/Dashboard";
import { getPageUrl, role, expectElementExists } from "./TestUtils";

const profilePage = new Profile();
const dashboard = new Dashboard();

/** Log in using specified email and password then navigate to learner profile page */
export async function navigateToLearnerProfile(testController) {
    await testController.useRole(role)
    .click(dashboard.profile)
    .expect(getPageUrl()).contains("learner/profile", "log in and reach profile page");

}

/** Assert that the toast that should be displayed after updating profile details exists. */
export async function testToast(testController) {
    await expectElementExists(testController, profilePage.successToast, "displaying \"Profile details updated\" toast");
}
