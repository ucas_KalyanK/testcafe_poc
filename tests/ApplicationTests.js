import { goToPage, getPageUrl, role, enterText, pasteText, selectDropdownOption, expectElementExists, expectElementAbsent, expectElementVisible, clickLink } from "./TestUtils";
import Dashboard from "../pages/Dashboard";
import CourseDashboard from "../pages/CourseDashboard";
import Search from "../pages/Search";
import SearchResults from "../pages/SearchResults";
import CourseDetails from "../pages/CourseDetails";
import Apply from "../pages/Apply";
import PersonalStatement from "../pages/PersonalStatement";
import ProviderQuestions from "../pages/ProviderQuestions";
import Reference from "../pages/Reference";
import AgentDetails from "../pages/AgentDetails";
import Finance from "../pages/Finance";
import config from "../config";
import dummytext from "../dummytext";
import * as AddressTests from "./AddressTests";
import { Selector } from "testcafe";

const dashboard = new Dashboard();
const courseDashboard = new CourseDashboard();
const searchPage = new Search();
const searchResultsPage = new SearchResults();
const courseDetailsPage = new CourseDetails();
const applyPage = new Apply();
const personalStatement = new PersonalStatement();
const providerQuestions = new ProviderQuestions();
const reference = new Reference();
const agentDetails = new AgentDetails();
const finance = new Finance();

/** Log in and navigate to the course search page. */
export async function navigateToCourseSearch(testController) {
    await testController.useRole(role)
    .click(dashboard.apply)
    .expect(getPageUrl()).contains("learner/dashboard", "navigate to courses dashboard page")
    .click(courseDashboard.search)
    .expect(getPageUrl()).contains("search", "navigate to search page");
}

/**
 * Search for the specified course using the search bar then assert that the current page is now the search results page.
 * @param {Object} testController the test controller object from the test
 * @param {Number} type the index of the desired course type in the courseTypes array in Search.js
 * @param {String} searchText the text to enter into the search bar
 */
export async function testSearchForCourse(testController, type, searchText) {
    //Select courses as the search type
    await selectDropdownOption(testController, searchPage.searchFirst, "Courses", "courses", "selecting courses as search type");
    //Assert that the secondary dropdown box now exists
    await expectElementExists(testController, searchPage.searchSecond, "secondary dropdown search box appearing");
    //Select the specified course type from the secondary dropdown box
    await selectDropdownOption(testController, searchPage.searchSecond, searchPage.courseTypes[type][0], searchPage.courseTypes[type][1],
        `selecting ${searchPage.courseTypes[type][0]} as a course type`);
    //Enter the search term into the text field
    await enterText(testController, searchPage.searchBox, searchText, `entering \"${searchText}\" into the search box`);
    //Press the submit button and assert that the current page is now the results page
    await clickLink(testController, searchPage.submit, "results", "pressing the submit button");
}


/**
 * Shortlists all of the specified courses then navigate to the dashboard to check that they have been shortlisted.
 * @param {Object} testController the test controller object from the test
 * @param {String} provider the name of the course provider
 * @param {String[]} courses the names of the courses to shortlist
 */
export async function testShortlistCourses(testController, provider, courses) {
    let providerContainer = searchResultsPage.providerContainer(provider);
    let moreCourses = searchResultsPage.moreCourses(providerContainer);
    //If the more courses button exists for the specified provider, press it and assert that it has disappeared
    if(await moreCourses.exists) {
        await testController.click(moreCourses).expect(moreCourses.visible).notOk("clicking \"show more courses\" button");
    }
    
    //Shortlist each of the specified courses
    for(var course of courses) {
        let courseContainer = searchResultsPage.courseContainer(providerContainer, course);
        let toUnshortlist = searchResultsPage.removeShortlist(courseContainer);
        let toShortlist = searchResultsPage.addShortlist(courseContainer);
        //If the course is already shortlisted, un-shortlist it first
        if(await toUnshortlist.visible) {
            await testController.click(toUnshortlist).expect(toShortlist.exists).ok("removing course from shortlist");
        }
        //Press the shortlist button
        await testController.click(toShortlist).expect(toUnshortlist.exists).ok("adding course to shortlist");
    }

    //Return to the dashboard to check that the courses are present in the shortlist
    await testController.navigateTo(`${config.baseUrl}/search/dashboard`);
    //Check if all courses are shown in shortlist
    for(var course of course) {
        testController.expect(dashboard.shortlistedCourseText(course).exists).ok(`shortlisting the course \"${course}\"`);
    }
}


/**
 * 
 * @param {Object} testController 
 * @param {String} course 
 */
export async function testUnshortlistFromDashboard(testController, course) {
    await goToPage(testController, `${config.baseUrl}/search/dashboard`);
    await clickLink(testController, dashboard.courseViewButton(course), "/courses/details", `viewing the course \"${course}\" from the shortlist`);
    await expectElementVisible(testController, courseDetailsPage.removeFromShortlist, "finding the remove from shortlist button");
    
    await testController.click(courseDetailsPage.removeFromShortlist)
    .expect(courseDetailsPage.removeFromShortlist.visible).notOk("removing course from shortlist")
    .expect(courseDetailsPage.addToShortlist.visible).ok("add to shortlist button appearing");

    await goToPage(testController, `${config.baseUrl}/search/dashboard`);

    await expectElementAbsent(testController, dashboard.shortlistedCourseText(course), "removing course from shortlist in dashboard");
}


/**
 * 
 * @param {Object} testController 
 * @param {String} course 
 */
export async function testApplyFromShortlist(testController, course) {
    //Return to the dashboard
    await goToPage(testController, `${config.baseUrl}/search/dashboard`);
    //Press the view button
    await clickLink(testController, dashboard.courseViewButton(course), "/courses/details", `viewing the course \"${course}\" from the shortlist`);
    //Press the apply button
    await clickLink(testController, courseDetailsPage.applyToCourse, "/learner/applications", "clicking the apply button");
}


export async function testPersonalStatement(testController) {
    //Click the personal statement link
    await clickLink(testController, applyPage.personalStatement, "/personal-statement", "reaching personal statement page");
    //Enter text into the text field
    await pasteText(testController, personalStatement.statementField, dummytext.essay, "entering text into the personal statement field");
    
    await testController
    //Save a draft
    .click(personalStatement.saveDraft)
    //Assert that the success toast has appeared
    .expect(personalStatement.successToast.exists).ok("displaying toast")
    //Click the preview button
    .click(personalStatement.openPreview)
    //Assert that the preview has appeared
    .expect(personalStatement.previewText.exists).ok("displaying preview")
    //Click the complete button
    .click(personalStatement.completePreview)
    //Assert that the toast and the edit button have appeared
    .expect(personalStatement.successToast.exists).ok("displaying toast")
    .expect(personalStatement.edit.exists).ok("displaying edit button")
    //Wait 5000ms to give the toast time to disappear
    .wait(5000)
    //Click the link back to the application summary
    .click(personalStatement.backLink)
    //Assert that the URL is correct
    .expect(getPageUrl()).notContains("/personal-statement", "leaving personal statement page")
    .expect(getPageUrl()).contains("/learner/applications", "returning to application page");
}


export async function testProviderQuestions(testController) {
    await clickLink(testController, applyPage.providerQuestions, "/provider-questions", "reaching provider questions page");
    await enterText(testController, providerQuestions.furtherComments, dummytext.doubleSentence, "entering text into the further comments section");

    // await testController
    // .setFilesToUpload("span _ngcontent-c0", [ "../portfolio/file1.txt" ])
    // .click(providerQuestions.selectFile);

    await testController.click(Selector(".button--primary").withText("Save"));
}


export async function testReference(testController) {
    // await clickLink(testController, applyPage.references, "/reference-requests", "reaching references page");
    
    // await testController
    // .expect(getPageUrl()).notContains("/reference-requests", "leaving references page")
    // .expect(getPageUrl()).contains("/learner/applications", "returning to application page");
}


export async function testAgentDetails(testController) {
    //Follow the link to the agent details page
    await clickLink(testController, applyPage.agentDetails, "/agent", "reaching agent details page");
    //Select "yes" from the using an agent dropdown box
    await selectDropdownOption(testController, agentDetails.usingAnAgent, agentDetails.booleanOptions[0][0], agentDetails.booleanOptions[0][1],
        "selecting an option from the using an agent dropdown box");
    //Assert that the is agent applying dropdown box has appeared
    await expectElementExists(testController, agentDetails.isAgentApplying, "agent details elements appearing");
    //Select an option from the is agent applying dropdown box
    await selectDropdownOption(testController, agentDetails.isAgentApplying, agentDetails.booleanOptions[1][0], agentDetails.booleanOptions[1][1],
        "selecting an option from the is agent applying dropdown box");
    //Enter text into the agency name field
    await enterText(testController, agentDetails.agencyName, "Brilliant Agency, inc", "entering agency name");
    //Enter text into the adviser name field
    await enterText(testController, agentDetails.adviserName, "Blah blah", "entering adviser name");
    //Enter text into the agent code field
    await enterText(testController, agentDetails.agentCode, "007", "entering agent code");
    //Enter text into the agent phone number field
    await enterText(testController, agentDetails.agentPhoneNumber, "01234567891", "entering agent phone number");
    //Enter text into the agent email address field
    await enterText(testController, agentDetails.agentEmail, "T.Panton@ucas.ac.uk", "entering agent email address")
    //Fill in the agent address
    await AddressTests.ukAddress(testController, agentDetails.addressType, agentDetails.postcode, agentDetails.findAddress, agentDetails.addressResults,
        agentDetails.line1, agentDetails.line2, agentDetails.line3, agentDetails.line4);
    //Save the agent details
    await testController.click(Selector(".button--primary").withText("Save"));
}


export async function testFinance(testController) {
    //Follow the link to the finance and funding page
    await clickLink(testController, applyPage.finance, "/funding", "reaching finance & funding page");
    //Try all of the funding options
    for(var option of finance.fundingOptions) {
        await selectDropdownOption(testController, finance.fundingType, option[0], option[1], `selecting \"${option[0]}\" as a funding type`);
    }
    //Save the funding details
    await testController.click(Selector(".button--primary").withText("Save"));
}

/** Log in and navigate to the course search page. */
export async function navigateToSearchCourse(testController) {
    await testController.useRole(role)

    .click(dashboard.postGraduates)
    .click(dashboard.apply)
    .expect(getPageUrl()).contains("learner/dashboard", "navigate to courses dashboard page")
    .click(courseDashboard.search)
    .expect(getPageUrl()).contains("search", "navigate to search page");
}

/**
 * Search for the specified course using the search bar then assert that the current page is now the search results page.
 * @param {Object} testController the test controller object from the test
 * @param {Number} type the index of the desired course type in the courseTypes array in Search.js
 * @param {String} searchText the text to enter into the search bar
 */
export async function enterSearchWord(testController, type, searchText) {
    //Enter the search term into the text field
    await enterText(testController, searchPage.searchBox, searchText, `entering \"${searchText}\" into the search box`);
    //Press the submit button and assert that the current page is now the results page
    await clickLink(testController, searchPage.submit, "results", "pressing the submit button");
}

export async function filter(testController) {
    await testController.click(dashboard.radPostGraduates)
    await testController.click(dashboard.chkAppliedThroughUCAS)
    
    await testController.click(dashboard.applyCourse)
    await testController.click(dashboard.courseOption)
    await testController.click(dashboard.applyApplication)

}

