import PersonalDetails from "../pages/PersonalDetails";
import { enterText, selectDropdownOption } from "./TestUtils";
import config from "../config";


const personalDetailsPage = new PersonalDetails();


/** Test each of the titles in order. */
export async function testTitles(testController) {
    //Wait 500ms. Clicking the titles dropdown too soon after the page loads seems to prevent it from opening,
    //causing TestCafe to crash
    testController.wait(500);

    //Select each title
    for(var item of personalDetailsPage.titles) {
        await selectDropdownOption(testController, personalDetailsPage.title, item[0], item[1], `selecting ${item[0]} as a title`);
    }
}

/** Test each of the name fields: first name, last name, preious name(s) and preferred name. */
export async function testNames(testController) {
    //First name
    await enterText(testController, personalDetailsPage.firstName, config.firstName, "entering a name into the first name field");
    //Last name
    await enterText(testController, personalDetailsPage.lastName, config.familyName, "entering a name into the last name field");
    //Previous name(s)
    await enterText(testController, personalDetailsPage.previousNames, config.previousName, "entering a name into the previous name(s) field");
    //Preferred name
    await enterText(testController, personalDetailsPage.preferredName, config.preferredName, "entering a name into the preferred name field");
}

/** Test each of the genders from the gender dropdown box. */
export async function testGenders(testController) {
    //Select each gender
    for(var item of personalDetailsPage.genders) {
        await selectDropdownOption(testController, personalDetailsPage.gender, item[0], item[1], `selecting ${item[0]} as a gender`)
    }
}

/** Test the date of birth day, month and year dropdown boxes. */
export async function testDOB(testController) {
    let dayIndex = 5, monthIndex = 2, yearIndex = 14;

    await testController
    //Test day
    .click(personalDetailsPage.dobDay)
    .click(personalDetailsPage.dobDayOptions.withText(personalDetailsPage.days[dayIndex][0]))
    .expect(personalDetailsPage.dobDay.value).eql(personalDetailsPage.days[dayIndex][1], "selecting a day for date of birth")
    //Test month
    .click(personalDetailsPage.dobMonth)
    .click(personalDetailsPage.dobMonthOptions.withText(personalDetailsPage.months[monthIndex][0]))
    .expect(personalDetailsPage.dobMonth.value).eql(personalDetailsPage.months[monthIndex][1], "selecting a month for date of birth")
    //Test year
    .click(personalDetailsPage.dobYear)
    .click(personalDetailsPage.dobYearOptions.withText(personalDetailsPage.years[yearIndex][0]))
    .expect(personalDetailsPage.dobYear.value).eql(personalDetailsPage.years[yearIndex][1], "selecting a year for date of birth");
}

