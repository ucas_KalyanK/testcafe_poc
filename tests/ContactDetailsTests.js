import ContactDetails from "../pages/ContactDetails";
import { enterText, selectDropdownOption } from "./TestUtils";
import * as AddressTests from "./AddressTests";
import config from "../config";
import { Selector } from "testcafe";


const contactDetailsPage = new ContactDetails();

const UK_TYPE = 0, NON_UK_TYPE = 1, BFPO_TYPE = 2;


/** Select UK address as the address type then enter a postcode. */
export async function testUKAddress(testController) {

    await AddressTests.ukAddress(testController, contactDetailsPage.addressType, contactDetailsPage.postcode, contactDetailsPage.findAddress,
        contactDetailsPage.addressResults, contactDetailsPage.addressLine1, contactDetailsPage.addressLine2, contactDetailsPage.addressLine3,
        contactDetailsPage.addressLine4);

    // await selectDropdownOption(testController, contactDetailsPage.addressType, contactDetailsPage.addressTypes[UK_TYPE][0],
    //     contactDetailsPage.addressTypes[UK_TYPE][1], "selecting UK address type");

    // await testPostcode(testController);
}

/** Select non-UK as the address type. */
export async function testNonUKAddress(testController) {
    await selectDropdownOption(testController, contactDetailsPage.addressType, contactDetailsPage.addressTypes[NON_UK_TYPE][0],
        contactDetailsPage.addressTypes[NON_UK_TYPE][1], "selecting non-UK address type");
}

/** Enter the postcode specified in config.json then select an address. */
async function testPostcode(testController) {
    //Enter the postcode into the text field
    await enterText(testController, contactDetailsPage.postcode, config.postcode, "entering a postcode");
    //Click the "find address" button
    await testController.click(contactDetailsPage.findAddress)
    .expect(contactDetailsPage.addressResults.exists).ok("pressing find address");
    //Click the address result and assert that the address is displayed
    await testController.click(contactDetailsPage.addressResults)
    .expect(contactDetailsPage.addressLine1.exists).ok("displaying address line 1")
    .expect(contactDetailsPage.addressLine2.exists).ok("displaying address line 2")
    .expect(contactDetailsPage.addressLine3.exists).ok("displaying address line 3")
    .expect(contactDetailsPage.addressLine4.exists).ok("displaying address line 4")
    .expect(contactDetailsPage.addressLine1.value).match(/\S/, "adding address in line 1")
    .expect(contactDetailsPage.addressLine2.value).match(/\S/, "adding address in line 2")
    .expect(contactDetailsPage.addressLine3.value).match(/\S/, "adding address in line 3");
}

/** Enter the telephone numbers from config.json as the mobile telephone number and the other telephone number. */
export async function testTelephoneNumbers(testController) {
    await enterText(testController, contactDetailsPage.mobilePhoneNo, config.mobileNumber, "entering a mobile telephone number");
    await enterText(testController, contactDetailsPage.otherPhoneNo, config.otherNumber, "entering an other telephone number");
}
