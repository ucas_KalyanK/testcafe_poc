import { ClientFunction, Selector, Role } from "testcafe";
import config from "../config";

var email, password;

//Attempt to read login details from login.json. If it doesn't exist, use the BBT super user account.
//Login file should have two attributes: email and password.
try {
    const login = require("../login");
    email = login.email;
    password = login.password;
} catch(exception) {
    console.log("Create a login.json file to specify login details.");
    email = "BBTSuperUser@ucas.ac.uk";
    password = "F1ndMyBugz!";
}

/** The role that should be used to log in. This should be passed into the setRole method of the test controller in every test in order to
 * remain logged in. */
export const role = Role(
  `${config.baseUrl}/Account/Login?returnUrl=https%3a%2f%2fsit-digital.ucasenvironments.com%2fsearch%2fdashboard`, async testController => {
      await testController
      .maximizeWindow()

      .typeText('#Email', email)
      .typeText('#Password', password)
      .click('#LoginSubmit')
  }, { preserveUrl: true }
);

/** Returns the current URL. This can be used for testing that the test user is currently on the correct page. */
export const getPageUrl = ClientFunction(() => window.location.href);

/** Returns a promise for checking that the design framework has loaded. */
export const designFrameworkReady = ClientFunction(() => {
    return new Promise(resolve => {
      UCASDesignFramework.promise.then(resolve)
    })
});

/**
 * Navigates to the page at the specified URL unless the page is already open.
 * @param {Object} testController the test controller object from the test
 * @param {String} url the URL to navigate to
 */
export async function goToPage(testController, url) {
    if(await getPageUrl() != url) {
        await testController.navigateTo(url);
    } else {
        console.log(`Already at ${url}`);
    }
}

/**
 * Asserts that a specific element exists on the page.
 * @param {Object} testController the test controller object from the test
 * @param {Object} selector the CSS selector to check exists
 * @param {String} message the message that should be displayed if this test fails
 */
export async function expectElementExists(testController, selector, message) {
    await testController.expect(selector.exists).ok(message);
}

/**
 * Asserts that a specific element does not exist on the page.
 * @param {Object} testController the test controller object from the test
 * @param {Object} selector the CSS selector to check if absent
 * @param {String} message the message that should be displayed if this test fails
 */
export async function expectElementAbsent(testController, selector, message) {
    await testController.expect(selector.exists).notOk(message);
}

/**
 * Asserts that a specific element is visible on the page.
 * @param {Object} testController the test controller object from the test
 * @param {Object} selector the CSS selector to check if visible
 * @param {String} message the message that should be displayed if this test fails
 */
export async function expectElementVisible(testController, selector, message) {
    await testController.expect(selector.visible).ok(message);
}

/**
 * Asserts that a specific element is not visible on the page.
 * @param {Object} testController the test controller object from the test
 * @param {Object} selector the CSS selector to check if invisible
 * @param {String} message the message that should be displayed if this test fails
 */
export async function expectElementInvisible(testController, selector, message) {
    await testController.expect(selector.visible).notOk(message);
}

/**
 * Enter text into the text field then assert that the text is present. Will overwrite any existing text.
 * @param {Object} testController the test controller object from the test
 * @param {Object} selector the CSS selector to identify the text field
 * @param {String} text the text to enter into the text field
 * @param {String} message the message that should be displayed if this test fails
 */
export async function enterText(testController, selector, text, message) {
    await testController
    .typeText(selector, text, { replace: true })
    .expect(selector.value)
    .eql(text, message);
}

/**
 * Instantly enter text into the text field then assert that the text is present. Will overwrite any existing text.
 * @param {Object} testController the test controller object from the test
 * @param {Object} selector the CSS selector to identify the text field
 * @param {String} text the text to enter into the text field
 * @param {String} message the message that should be displayed if this test fails
 */
export async function pasteText(testController, selector, text, message) {
    await testController
    .typeText(selector, text, { replace: true, paste: true })
    .expect(selector.value)
    .eql(text, message);
}

/**
 * Enter text into the text field without overwriting the previous value. insertPos may be excluded when calling to append to
 * the end of the text.
 * @param {Object} testController the test controller object from the test
 * @param {Object} selector the CSS selector to identify the text field
 * @param {String} text the text to enter into the text field
 * @param {String} expected the expected value of the text field after the new text has been appended
 * @param {String} message the message that should be displayed if this test fails
 * @param {Number} insertPos the position in the text at which the text should be inserted
 */
export async function appendText(testController, selector, text, expected, message, insertPos) {
    let options = insertPos === undefined ? { replace: false } : { replace: false, caretPos: insertPos };
    await testController
    .typeText(selector, text, options)
    .expect(selector.value)
    .eql(expected, message);
}

/**
 * Opens the dropdown box and selects the specified option, then asserts that that option has been selected.
 * @param {Object} testController the test controller object from the test
 * @param {Object} selector the CSS selector to identify the dropdown box
 * @param {String} option the exact name of the option that should be selected
 * @param {String} optionValue the value attribute of the option that should be expected
 * @param {String} message the message that should be displayed if the test fails
 */
export async function selectDropdownOption(testController, selector, option, optionValue, message) {
    await testController
    .click(selector)
    .click(selector.find("option").withText(option))
    .expect(selector.value).eql(optionValue, message);
}

/**
 * Clicks the specified element and asserts that the page URL now contains the specified string.
 * @param {Object} testController the test controller object from the test
 * @param {Object} selector the CSS selector to identify the element that should be clicked
 * @param {String} expectedUrl part of the URL unique to the expected page, e.g. "/learner/profile" for the profile page
 * @param {String} message the message that should be displayed if the test fails
 */
export async function clickLink(testController, selector, expectedUrl, message) {
    await testController
    .click(selector)
    .expect(getPageUrl())
    .contains(expectedUrl, message);
}

/**
 * Clicks the save button and asserts that the current page is now the profile page.
 * @param {Object} testController the test controller object from the test
 * @param {String} page a unique part of the current page URL that will be asserted that the URL no longer contains
 * @param {String} leaveMessage the message that should be displayed if leaving the page fails
 * @param {String} returnToProfileMessage the message that should be displayed if reaching the profile page fails
 */
export async function saveLearnerDetails(testController, page, leaveMessage, returnToProfileMessage) {
  //Click the save button
  await testController.click(Selector(".button--primary").withText("Save"))
  //Check if no longer on the previous page
  .expect(getPageUrl()).notContains(page, leaveMessage)
  //Check if on profile page
  .expect(getPageUrl()).contains("learner/profile", returnToProfileMessage);
}
