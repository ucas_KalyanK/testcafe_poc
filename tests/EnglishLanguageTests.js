import EnglishLanguage from "../pages/EnglishLanguage";
import { enterText, selectDropdownOption, expectElementExists } from "./TestUtils";


const englishLanguagePage = new EnglishLanguage();


export async function testRadioButtons(testController) {
    testController.click(englishLanguagePage.yes).click(englishLanguagePage.no);
}
