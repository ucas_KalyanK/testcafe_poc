import SupportingInformation from "../pages/SupportingInformation";
import { enterText, selectDropdownOption, expectElementExists } from "./TestUtils";
import config from "../config";


const supportingInformationPage = new SupportingInformation();


/** Select "yes" from the "have you been in care" dropdown box, assert that the care duration dropdown box has appeared then select
* an item from the care duration dropdown box. */
export async function testBeenInCare(testController) {
    //Select "yes" from the dropdown box
    selectDropdownOption(testController, supportingInformationPage.beenInCare, supportingInformationPage.booleanOptions[0][0],
        supportingInformationPage.booleanOptions[0][1], "selecting been in care");
    //Assert that the duration dropdown box has appeared
    expectElementExists(testController, supportingInformationPage.careDuration, "duration of time in care option appearing");
    //Select the first item from the duration dropdown box
    selectDropdownOption(testController, supportingInformationPage.careDuration, supportingInformationPage.timeInCareOptions[0][0],
        supportingInformationPage.timeInCareOptions[0][1], "selecting a duration in care");
}

/** Select an item from the care responsibility dropdown box. */
export async function testCareResponsibilities(testController) {
    selectDropdownOption(testController, supportingInformationPage.careResponsibility, supportingInformationPage.booleanOptions[1][0],
        supportingInformationPage.booleanOptions[1][1], "selecting care responsibility");
}

/** Select an item from the parental responsibility dropdown box. */
export async function testParentingResponsibility(testController) {
    selectDropdownOption(testController, supportingInformationPage.parentalResponsibility, supportingInformationPage.booleanOptions[1][0],
        supportingInformationPage.booleanOptions[1][1], "selecting parenting responsibility");
}

/** Select an item from the refugee status dropdown box. */
export async function testRefugeeStatus(testController) {
    selectDropdownOption(testController, supportingInformationPage.refugeeStatus, supportingInformationPage.refugeeOptions[0][0],
        supportingInformationPage.refugeeOptions[0][1], "selecting refugee status");
}

/** Select an item from the disability type dropdown box, assert that the extra information text field has appeared then enter text
* into the extra information field. */
export async function testDisability(testController) {
    selectDropdownOption(testController, supportingInformationPage.disabilityType, supportingInformationPage.disabilityOptions[1][0],
        supportingInformationPage.disabilityOptions[1][1], "selecting disability type");
    
    expectElementExists(testController, supportingInformationPage.disabilityInformation, "disability further information field appearing");

    enterText(testController, supportingInformationPage.disabilityInformation,
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur lobortis dolor risus, eu vestibulum magna efficitur sed.",
        "entering text into the disability further information field");
}

/** Select an item from the guardian higher education field. */
export async function testHE(testController) {
    selectDropdownOption(testController, supportingInformationPage.guardianHE, supportingInformationPage.heOptions[2][0],
        supportingInformationPage.heOptions[2][1], "selecting guardian higher education");
}
