import Profile from "../pages/Profile"
import PersonalDetails from "../pages/PersonalDetails";
import * as ProfileTests from "./ProfileTests"
import * as PersonalDetailsTests from "./PersonalDetailsTests";
import * as ContactTests from "./ContactDetailsTests";
import * as SupporingInfoTests from "./SupportingInformationTests";
import * as EnglishTests from "./EnglishLanguageTests";
import * as ApplicationTests from "./ApplicationTests";
import * as AddressTests from "./AddressTests";
import { goToPage, designFrameworkReady, clickLink, saveLearnerDetails } from "./TestUtils";
import config from "../config";


const profilePage = new Profile();
const personalDetailsPage = new PersonalDetails();


//The fixture which contains all of the tests that should be run
fixture("End to end test")
//Use the dashboard as the default page
.page(`${config.baseUrl}/search/dashboard`)
//Check that the design framework is ready before running each test
.beforeEach(async testController => {
    await designFrameworkReady();
});

/*
//Tests for the personal details page
test("Personal details", async testController => {
    //Log in and navigate to the profile page
    await ProfileTests.navigateToLearnerProfile(testController);
    //Follow the personal details link
    await clickLink(testController, profilePage.personalDetails, "personal-details", "reaching personal details page");
    //Test each of the titles
    await PersonalDetailsTests.testTitles(testController);
    //Test the first and last name fields
    await PersonalDetailsTests.testNames(testController);
    //Test each of the genders
    await PersonalDetailsTests.testGenders(testController);
    //Test date of birth
    await PersonalDetailsTests.testDOB(testController);
    //Test saving the personal details
    await saveLearnerDetails(testController, "personal-details", "leaving the personal details page", "returning to profile page");
    await ProfileTests.testToast(testController);
});


//Tests for the contact details page
test("Contact details", async testController => {
    //Return to the profile page
    await ProfileTests.navigateToLearnerProfile(testController);
    //Follow the contact details link
    await clickLink(testController, profilePage.contactDetails, "contact-details", "reaching contact details page");
    //Test mobile telephone number and other telephone number
    await ContactTests.testTelephoneNumbers(testController);
    //Test filling in a UK address
    await ContactTests.testUKAddress(testController);

    //Save the contact details
    await saveLearnerDetails(testController, "contact-details", "leaving the contact details page", "returning to profile page");
});


//Tests for the supporting information page
test("Supporting information", async testController => {
    //Return to the profile page
    await ProfileTests.navigateToLearnerProfile(testController);
    //Follow the supporting information link
    await clickLink(testController, profilePage.support, "support", "reaching supporting information page");
    //Test each of the elements
    await SupporingInfoTests.testBeenInCare(testController);
    await SupporingInfoTests.testCareResponsibilities(testController);
    await SupporingInfoTests.testParentingResponsibility(testController);
    await SupporingInfoTests.testRefugeeStatus(testController);
    await SupporingInfoTests.testDisability(testController);
    await SupporingInfoTests.testHE(testController);
    //Save the supporting information
    await saveLearnerDetails(testController, "support", "leaving the supporting information page", "returning to profile page");
});


//Tests for English language page
test("English language", async testController => {
    //Return to the profile page
    await ProfileTests.navigateToLearnerProfile(testController);
    //Follow the English language link
    await clickLink(testController, profilePage.english, "english-language", "reaching english language page");
    //Test the elements
    await EnglishTests.testRadioButtons(testController);
    //Return to the profile page
    goToPage(testController, `${config.baseUrl}/learner/profile`);
});


// test("Residency", async testController => {
//
// });

//Tests for shortlisting and applying to courses
test("Applying", async testController => {
    //Navigate to the course search page
    await ApplicationTests.navigateToCourseSearch(testController);
    //Search for "University of UCAS"
    await ApplicationTests.testSearchForCourse(testController, 1, "University of UCAS");
    //Shortlist some courses from the Univeristy of UCAS
    await ApplicationTests.testShortlistCourses(testController, "University of UCAS", [
        "Welsh Studies",
        "Baking in the Victorian Era",
        "Pharmacology and Pharmaceutical Science Management"
    ]);
    //Remove a course from the shortlist
    await ApplicationTests.testUnshortlistFromDashboard(testController, "Baking in the Victorian Era");
    //Apply to a course
    await ApplicationTests.testApplyFromShortlist(testController, "Welsh Studies");
    //Personal statement
    await ApplicationTests.testPersonalStatement(testController);
    //Provider questions
    await ApplicationTests.testProviderQuestions(testController);
    //Reference
    await ApplicationTests.testReference(testController);
    //Agent details
    await ApplicationTests.testAgentDetails(testController);
    //Finance and funding
    await ApplicationTests.testFinance(testController);
});

*/

//Tests for search, applying to courses and status of application
test("ApplicationStatus", async testController => {
    //Navigate to the course search page

    await ApplicationTests.navigateToSearchCourse(testController);
    //Search for "University of UCAS"


    await ApplicationTests.enterSearchWord(testController, 1, "Edge Hill");
    //Filter with PG
    await ApplicationTests.filter(testController);

});
