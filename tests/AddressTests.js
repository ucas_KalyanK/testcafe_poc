import * as TestUtils from "./TestUtils";
import config from "../config";

/**
 * 
 * @param {Object} testController 
 * @param {Object} type 
 * @param {Object} postcode 
 * @param {Object} findAddress 
 * @param {Object} results 
 * @param {Object} line1 
 * @param {Object} line2 
 * @param {Object} line3 
 * @param {Object} line4 
 */
export async function ukAddress(testController, type, postcode, findAddress, results, line1, line2, line3, line4) {
    //Select UK address as the address type
    await TestUtils.selectDropdownOption(testController, type, "UK address", "10000", "selecting UK address type");
    //Assert that the postcode field exists
    await TestUtils.expectElementExists(testController, postcode, "postcode field appearing");
    //Enter the postcode into the postcode field
    await TestUtils.enterText(testController, postcode, config.postcode, "entering postcode");

    await testController
    //Click the find address button
    .click(findAddress)
    //Assert that the address results exist
    .expect(results.exists).ok("address results being displayed")
    //Click on the address result
    .click(results)
    //Assert that all 4 lines exist
    .expect(line1.exists).ok("displaying address line 1")
    .expect(line2.exists).ok("displaying address line 2")
    .expect(line3.exists).ok("displaying address line 3")
    .expect(line4.exists).ok("displaying address line 4")
    //Assert that the first 3 lines contain non-whitespace characters
    .expect(line1.value).match(/\S+/, "auto-filling address in line 1")
    .expect(line2.value).match(/\S+/, "auto-filling address in line 2")
    .expect(line3.value).match(/\S+/, "auto-filling address in line 3");
}
