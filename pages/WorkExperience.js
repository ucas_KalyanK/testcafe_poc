import { Selector } from "testcafe";

export default class WorkExperience {

    constructor() {
        this.yes                = Selector("#yes-button");
        this.no                 = Selector("#no-button");
        this.haveExperience     = Selector("#add-work-experience-secondary-button");
        this.noExperience       = Selector("#no-work-experience-button");
        this.addWorkExperience  = Selector("#add-work-experience-primary-button");

        this.roleTitle          = Selector("#roleTitle");
        this.organisation       = Selector("#organisationName");
        this.contactNumber      = Selector("#contactNumber");
        this.fullTime           = Selector("#isFullTime_true");
        this.partTime           = Selector("#isFullTime_false");
        this.summary            = Selector("#summaryOfResponsibilities");
    }

}
