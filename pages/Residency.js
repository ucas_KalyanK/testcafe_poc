import { Selector } from "testcafe";

export default class Residency {

    constructor() {
        this.countryOfBirth         = Selector("#countryOfBirth");
        this.nationality            = Selector("#nationality");
        this.addNationality         = Selector(".button").withText("Add another nationality");
        this.addressType            = Selector("#addressType");
        this.areaOfResidence        = Selector("#areaOfPermanentResidence");
        this.residentialStatus      = Selector("#residentialStatus");

        //Non UK nationality elements
        this.passportNumber         = Selector("#number");
        this.passportDay            = Selector("#day");
        this.passportMonth          = Selector("#month");
        this.passportYear           = Selector("#year");
        this.placeOfIssue           = Selector("#placeOfIssue");

        this.haveVisa               = Selector("#haveUKVisa");
        this.visaType               = Selector("#type");
        this.needStudentVisa        = Selector("#isStudyVisaRequired");
        this.whereApplyForVisa      = Selector("#plannedUKVisaApplicationCountry");
        this.hasVisaBeenDenied      = Selector("#hasPastUKVisaBeenDenied");
        this.whyVisaDenied          = Selector("#refusalReason");
        this.studiedWithVisa        = Selector("#havePreviouslyStudiedInUKWithTier4Visa");
        this.visaSponsor            = Selector("#sponsoringInstitutionName");
        this.studyLevel             = Selector("#studyLevel");
        this.courseCompleted        = Selector("#courseCompleted");
        this.leftUKAfterStudy       = Selector("#leftUKAfterStudyFinished");

        //Non UK address elements
        this.country                = Selector("#country");
    }

}