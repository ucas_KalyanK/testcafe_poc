import { Selector } from "testcafe";
import {xPathToCss } from "xpath-to-css";

export default class Dashboard {

    constructor() {
        this.profile        = Selector("#my-profile-button");
        this.apply          = Selector(".button").withText("Go to Apply");
        this.applications   = Selector("#my-applications-button");
        this.searchFirst    = Selector(".global-search__firstDropdown");
        this.searchSecond   = Selector(".global-search__secondDropdown");
        this.searchBox      = Selector("#SearchText");
        this.submit         = Selector("#SearchSubmit");
        this.postGraduates  = Selector('.tabs__tab').withText('Postgraduates');
        this.radPostGraduates  = Selector("#Destination_Postgraduate");
        this.chkAppliedThroughUCAS  = Selector("#Scheme_UCAS_Postgraduate_checkbox");
        this.applyCourse  = Selector(".course-details");
        this.courseOption  = Selector('[data-start-date="January 2020"]');
        this.applyApplication  = Selector('[data-gtm-action="Application Initiated - AMS"]');
        
        

    }

    shortlistedCourseText(name) {
        return Selector("p").withText(name);
    }

    courseViewButton(name) {
        return this.shortlistedCourseText(name).parent(0).find(".button").withText("View");
    }

}
