import { Selector } from "testcafe";

export default class EnglishLanguage {

    constructor() {
        this.yes    = Selector("#Primary_Yes");
        this.no     = Selector("#Primary_No");
    }

}
