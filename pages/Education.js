import { Selector } from "testcafe";

export default class Education {

    constructor() {
        this.addPOE             = Selector(".button--primary").withText("Add place of education");
        this.educationType      = Selector("#type");
        this.name               = Selector("#name");
        this.country            = Selector("#countryOfStudy");
        this.startMonth         = Selector("#startDate_month");
        this.startYear          = Selector("#startDate_year");
        this.endMonth           = Selector("#endDate_month");
        this.endYear            = Selector("#endDate_year");
        this.fullTime           = Selector("#fullTime");
        this.partTime           = Selector("#partTime");
        this.yesQualifications  = Selector("#yes");
        this.noQualifications   = Selector("#no");

        this.educationTypes = [
            [ "School/College/University",  "0" ],
            [ "Online study",               "1" ],
            [ "Work-based study",           "2" ],
            [ "Home schooled",              "3" ]
        ];
    }

}