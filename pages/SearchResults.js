import { Selector } from "testcafe";

export default class SearchResults {

    constructor() {
        this.searchFirst        = Selector(".global-search__firstDropdown");
        this.searchSecond       = Selector(".global-search__secondDropdown");
        this.searchBox          = Selector("#SearchText");
        this.submit             = Selector("#SearchSubmit");
        this.searchResults      = Selector(".accordion__inner-wrapper").child("div").child("ul");
    }

    providerContainer(provider) {
        return Selector("h3").withText(provider).parent(1);
    }

    moreCourses(providerContainer) {
        return providerContainer.find(".show-more-courses");
    }

    courseContainer(providerContainer, course) {
        return providerContainer.find("strong").withText(course).parent(4);
    }

    addShortlist(courseContainer) {
        return courseContainer.find(".js-shortlist-add");
    }

    removeShortlist(courseContainer) {
        return courseContainer.find(".js-shortlist-remove");
    }

}