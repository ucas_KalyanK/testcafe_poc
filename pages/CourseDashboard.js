import { Selector } from "testcafe";

export default class CourseDashboard {

    constructor() {
        this.search     = Selector(".button--primary").withText("Search for a course");
    }

}
