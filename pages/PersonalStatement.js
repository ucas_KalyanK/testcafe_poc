import { Selector } from "testcafe";

export default class PersonalStatement {

    constructor() {
        this.statementField         = Selector("#statement");
        this.saveDraft              = Selector(".button").withText("Save draft");
        this.openPreview            = Selector("#preview-button");
        this.closePreview           = Selector(".button").withText("Close preview");
        this.previewText            = Selector("#preview").find("span");
        this.complete               = Selector("#complete-button");
        this.completePreview        = Selector(".button--primary").withText("Mark as complete");
        this.edit                   = Selector("#editStatement");
        this.successToast           = Selector(".toast-success");
        this.backLink               = Selector(".context-bar__back-link").withText("Application summary");
    }

}