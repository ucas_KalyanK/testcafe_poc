import { Selector } from "testcafe";

export default class SupportingInformation {

    constructor() {
        this.beenInCare             = Selector("#haveBeenInCare");
        this.careResponsibility     = Selector("#haveCareResponsibility");
        this.parentalResponsibility = Selector("#haveParentalCareResponsibility");
        this.refugeeStatus          = Selector("#refugeeStatus");
        this.disabilityType         = Selector("#disabilityType");
        this.guardianHE             = Selector("#guardianHigherEducated");

        //Been in care questions
        this.careDuration           = Selector("#durationInCare");

        //Disabilities questions
        this.disabilityInformation  = Selector("#disabilityInformation");

        
        this.booleanOptions = [
            [ "Yes",    "true"  ],
            [ "No",     "false" ]
        ];

        this.timeInCareOptions = [
            [ "Less than one week",  "10000" ],
            [ "1 week - 3 months",   "10001" ],
            [ "3 months - 1 year",   "10002" ],
            [ "1 year - 2 years",    "10003" ],
            [ "2 years - 3 years",   "10006" ],
            [ "3 years or more",     "10004" ]
        ];

        this.refugeeOptions = [
            [ "No",                                                                     "10000" ],
            [ "I am a refugee or have been awarded humanitarian protection",            "10001" ],
            [ "I am an asylum seeker or have limited or discretionary leave to remain", "10002" ]
        ];

        this.disabilityOptions = [
            [ "No disability",                                                                                                              "A" ],
            [ "You are blind or have a serious visual impairment uncorrected by glasses",                                                   "C" ],
            [ "You are deaf or have a serious hearing impairment",                                                                          "D" ],
            [ "You have a disability, impairment or medical condition that is not listed above",                                            "I" ],
            [ "You have a long standing illness or health condition such as cancer, HIV, diabetes, chronic heart disease, or epilepsy",     "E" ],
            [ "You have a mental health condition, such as depression, schizophrenia or anxiety disorder",                                  "F" ],
            [ "You have a social/communication impairment such as Asperger's syndrome/other autistic spectrum disorder",                    "B" ],
            [ "You have a specific learning difficulty such as dyslexia, dyspraxia or AD(H)D",                                              "G" ],
            [ "You have physical impairment or mobility issues, such as difficulty using your arms or using a wheelchair or crutches",      "H" ],
            [ "You have two or more impairments and/or disabling medical conditions",                                                       "J" ]
        ];

        this.heOptions = [
            [ "Yes",                    "10001" ],
            [ "No",                     "10002" ],
            [ "Don't know",             "10003" ],
            [ "I prefer not to say",    "10004" ]
        ];
    }

}