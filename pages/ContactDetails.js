import { Selector } from "testcafe";

/** Class containing data relevant to the contact details page. */
export default class ContactDetails {

    constructor() {
        this.addressType        = Selector("#addressType");
        this.mobilePhoneNo      = Selector("#mobilePhoneNumber");
        this.otherPhoneNo       = Selector("#otherPhoneNumber");

        this.addressLine1       = Selector("#line1");
        this.addressLine2       = Selector("#line2");
        this.addressLine3       = Selector("#line3");
        this.addressLine4       = Selector("#line4");

        //UK address
        this.postcode           = Selector("#postcode");
        this.findAddress        = Selector("#findAddressButton");
        this.addressResults     = Selector("#address-results");
       
        //Non-UK address
        this.country            = Selector("#country");

        //BFPO address
        this.bfpoNo             = Selector("#bfpoNumber");


        //All of the address types
        this.addressTypes = [
            [ "UK address",      "10000" ],
            [ "Non-UK address",  "10001" ],
            [ "BFPO address",    "10002" ]
        ];

    }

}