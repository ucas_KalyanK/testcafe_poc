import { Selector } from "testcafe"

export default class CourseDetails {

    constructor() {
        this.addToShortlist             = Selector(".js-shortlist-add");
        this.removeFromShortlist        = Selector(".js-shortlist-remove");
        this.applyToCourse              = Selector(".js-applicationServices-redirect");
        this.entryRequirements          = Selector("#entry-requirements-section");
    }

}