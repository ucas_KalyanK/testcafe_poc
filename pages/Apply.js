import { Selector } from "testcafe";

export default class Apply {

    constructor() {
        this.personalStatement      = Selector("#personalStatement");
        this.providerQuestions      = Selector("#providerQuestions");
        this.references             = Selector("#references");
        this.agentDetails           = Selector("#agentDetails");
        this.finance                = Selector("#funding");
    }

}
