import { Selector } from "testcafe";

export default class Profile {

    constructor() {
        this.personalDetails = Selector("#personalDetails");
        this.contactDetails  = Selector("#contactDetails");
        this.workExperience  = Selector("#workExperience");
        this.education       = Selector("#education");
        this.residency       = Selector("#residency");
        this.english         = Selector("#englishLanguage");
        this.support         = Selector("#support");
        this.successToast    = Selector(".toast-success").withText("Profile details updated.");
    }

}