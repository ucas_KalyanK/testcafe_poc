import { Selector } from "testcafe"

export default class Reference {

    constructor() {
        
        //First reference
        this.firstContainer         = Selector("#first-reference-tab1");
        this.firstTitle             = Selector("#first-reference-Title");
        this.firstForename          = Selector("#first-reference-Firstname"); //I avoided making the name of this variable firstFirstName
        this.firstFamilyName        = Selector("#first-reference-Lastname");
        this.firstEmail             = Selector("#first-reference-EmailAddress");
        this.firstPhoneNumber       = Selector("#first-reference-PhoneNumber");
        this.firstJobTitle          = Selector("#first-reference-JobTitle");
        this.firstOrganisation      = Selector("#first-reference-Organisation");
        this.firstConnection        = Selector("#first-reference-Relationship");
        //First reference address
        this.firstAddressType       = Selector("#first-reference-addressType");
        this.firstPostcode          = this.firstContainer.find("#postcode");
        this.firstFindAddress       = this.firstContainer.find("#findAddressButton");
        this.firstAddressResults    = this.firstContainer.find("#address-results");
        this.firstLine1             = Selector("#first-reference-line1");
        this.firstLine2             = Selector("#first-reference-line2");
        this.firstLine3             = Selector("#first-reference-line3");
        this.firstLine4             = Selector("#first-reference-line4");
        //End first reference address
        this.firstSave              = Selector("#first-reference-").find(".button").withText("Save");


        //Second reference
        this.secondContainer        = Selector("#second-reference-tab1");
        this.secondTitle            = Selector("#second-reference-Title");
        this.secondForename         = Selector("#second-reference-Firstname");
        this.secondFamilyName       = Selector("#second-reference-Lastname");
        this.secondEmail            = Selector("#second-reference-EmailAddress");
        this.secondPhoneNumber      = Selector("#second-reference-PhoneNumber");
        this.secondJobTitle         = Selector("#second-reference-JobTitle");
        this.secondOrganisation     = Selector("#second-reference-Organisation");
        this.secondConnection       = Selector("#second-reference-Relationship");
        //Second reference address
        this.secondAddressType      = Selector("#second-reference-addressType");
        this.secondPostcode         = this.secondContainer.find("#postcode");
        this.secondFindAddress      = this.secondContainer.find("#findAddressButton");
        this.secondAddressResults   = this.secondContainer.find("#address-results");
        this.secondLine1            = Selector("#second-reference-line1");
        this.secondLine2            = Selector("#second-reference-line2");
        this.secondLine3            = Selector("#second-reference-line3");
        this.secondLine4            = Selector("#second-reference-line4");
        //End second reference address
        this.secondSave             = Selector("#second-reference-").find(".button").withText("Save");

    }

}
