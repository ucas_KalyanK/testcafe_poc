import { Selector } from "testcafe";
import config from "../config";

export default class PersonalDetails {

    constructor() {
        this.title           = Selector("#title");
        this.firstName       = Selector("#firstName");
        this.lastName        = Selector("#familyName");
        this.previousNames   = Selector("#previousNames");
        this.preferredName   = Selector("#preferredName");
        this.gender          = Selector("#gender");
        this.dobDay          = Selector("#day");
        this.dobMonth        = Selector("#month");
        this.dobYear         = Selector("#year");
        this.backToProfile   = Selector(".context-bar__back-link").withText("Profile");
        this.saveButton      = Selector(".button--primary").withText("Save");

        this.dobDayOptions   = this.dobDay.find("option");
        this.dobMonthOptions = this.dobMonth.find("option");
        this.dobYearOptions  = this.dobYear.find("option");

        //All of the titles and the values of their respective option elements
        this.titles = [
            [ "Dr",      "1004" ],
            [ "Miss",    "1001" ],
            [ "Mr",      "1000" ],
            [ "Mrs",     "1003" ],
            [ "Ms",      "1002" ],
            [ "Mx",      "1005" ],
            [ "Prof",    "1035" ]
        ];

        //All of the genders and the values of their respective option elements
        this.genders = [
            [ "Female", "10001" ],
            [ "Male",   "10000" ],
            [ "Other",  "10004" ]
        ];

        this.days = [];
        for(let day = 1; day <= 31; day++) {
            let dayStr = day.toString();
            this.days.push([ day < 10 ? "0" + dayStr : dayStr, dayStr ]);
        }

        this.months = [];
        for(let month = 1; month <= 12; month++) {
            let monthStr = month.toString();
            this.months.push([ month < 10 ? "0" + monthStr : monthStr, (month - 1).toString() ]);
        }

        this.years = [];

        for(let year = config.firstYear; year <= config.latestYear; year++) {
            let yearStr = year.toString();
            this.years.push([ yearStr, yearStr ]);
        }

    }

}