import { Selector } from "testcafe";

export default class Search {

    constructor() {
        this.searchFirst    = Selector(".global-search__firstDropdown");
        this.searchSecond   = Selector(".global-search__secondDropdown");
        this.searchBox      = Selector("#SearchText");
        this.submit         = Selector("#SearchSubmit");
        this.dashboard      = Selector(".button").withText("Dashboard");

        this.courseTypes = [
            [ "Undergraduate",      "undergraduate"     ],
            [ "Postgraduate",       "postgraduate"      ],
            [ "Conservatoires",     "conservatoires"    ],
            [ "Teacher Training",   "uut"               ],
            [ "16-18 Choices",      "progress"          ],
            [ "Apprenticeships",    "apprenticeships"   ]
        ];
    }

}