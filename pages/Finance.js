import { Selector } from "testcafe";

export default class Finance {

    constructor() {
        this.fundingType = Selector("#method");
        
        this.fundingOptions = [
            [ "Career development loan",    "10003" ],
            [ "Employer",                   "10001" ],
            [ "Postgraduate loan",          "10002" ],
            [ "Scholarship",                "10004" ],
            [ "Self/Family",                "10000" ],
            [ "Other",                      "10005" ]
        ];
    }

}
