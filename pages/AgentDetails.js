import { Selector } from "testcafe";

export default class AgentDetails {

    constructor() {
        this.usingAnAgent       = Selector("#isAgentInUse");
        this.isAgentApplying    = Selector("#isAgentApplying");
        this.agencyName         = Selector("#agencyName");
        this.adviserName        = Selector("#adviserName");
        this.agentCode          = Selector("#agencyCode");
        this.agentPhoneNumber   = Selector("#phoneNumber");
        this.agentEmail         = Selector("#emailAddress");
        
        this.addressType        = Selector("#addressType");
        this.postcode           = Selector("#postcode");
        this.findAddress        = Selector("#findAddressButton");
        this.addressResults     = Selector("#address-results");
        this.line1              = Selector("#line1");
        this.line2              = Selector("#line2");
        this.line3              = Selector("#line3");
        this.line4              = Selector("#line4");

        this.booleanOptions = [
            [ "Yes",    "true"  ],
            [ "No",     "false" ]
        ];
    }

}
