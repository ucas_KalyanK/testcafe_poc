import { Selector } from "testcafe";

export default class PersonalStatement {

    constructor() {
        this.selectFile         = Selector(".button").withText("Select file");
        this.anotherFile        = Selector(".button").withText("Add another file");
        this.category           = Selector("#category_select");
        this.cancel             = Selector(".button").withText("Cancel");
        this.upload             = Selector(".button").withText("Upload");
        this.furtherComments    = Selector("textarea");
        this.backLink           = Selector(".context-bar__back-link").withText("Application summary");

        this.categories = [
            [ "Portfolio",                          "10000" ],
            [ "Research proposal",                  "10001" ],
            [ "Curriculum Vitae",                   "10002" ],
            [ "Essay or Assignment",                "10003" ],
            [ "Proof of funding or Funding source", "10004" ],
            [ "Other",                              "10005" ]
        ];
    }

}